/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/
#include "../CSCConditionsTestAlgMT.h"
#include "../MdtTwinTubeTestAlg.h"
#include "../MdtCalibTestAlg.h"
#include "../NswCondTestAlg.h"
#include "../NswDcsTestAlg.h"
#include "../NswPassivationTestAlg.h"
#include "../MdtCablingTestAlg.h"
#include "../RpcCablingTestAlg.h"
#include "../MMCablingTestAlg.h"
#include "../ALineInjectTestAlg.h"
#include "../TgcCondDbTestAlg.h"
#include "../TgcDigtThresholdTestAlg.h"
#include "../TgcDigtJitterTestAlg.h"

DECLARE_COMPONENT(ALineInjectTestAlg)
DECLARE_COMPONENT(Muon::MdtTwinTubeTestAlg)
DECLARE_COMPONENT(Muon::MdtCalibTestAlg)
DECLARE_COMPONENT(CSCConditionsTestAlgMT)
DECLARE_COMPONENT(NswCondTestAlg)
DECLARE_COMPONENT(NswDcsTestAlg)
DECLARE_COMPONENT(NswPassivationTestAlg)
DECLARE_COMPONENT(MdtCablingTestAlg)
DECLARE_COMPONENT(Muon::RpcCablingTestAlg)
DECLARE_COMPONENT(MMCablingTestAlg)
DECLARE_COMPONENT(TgcCondDbTestAlg)
DECLARE_COMPONENT(TgcDigtThresholdTestAlg)
DECLARE_COMPONENT(TgcDigtJitterTestAlg)