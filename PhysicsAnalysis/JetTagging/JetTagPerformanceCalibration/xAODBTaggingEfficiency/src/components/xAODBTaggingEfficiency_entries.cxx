

#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"
#include "xAODBTaggingEfficiency/BTaggingTruthTaggingTool.h"
#include "xAODBTaggingEfficiency/BTaggingEigenVectorRecompositionTool.h"
#include "xAODBTaggingEfficiency/BTaggingSelectionJsonTool.h"
#include "xAODBTaggingEfficiency/BTaggingEfficiencyJsonTool.h"

#include "../ToolTester.h"
// Should probably alter the namespace

DECLARE_COMPONENT( BTaggingEfficiencyTool )
DECLARE_COMPONENT( BTaggingSelectionTool )
DECLARE_COMPONENT( BTaggingTruthTaggingTool )
DECLARE_COMPONENT( BTaggingEigenVectorRecompositionTool )
DECLARE_COMPONENT( BTaggingSelectionJsonTool )
DECLARE_COMPONENT( BTaggingEfficiencyJsonTool )

DECLARE_COMPONENT( BTagToolTester )

